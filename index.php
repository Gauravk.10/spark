<?php

$servername = "localhost";
$username = "gaurav";
$password = "gaurav";
$dbname = "spark";
$port = "3306";

$conn = new mysqli($servername, $username, $password, $dbname, $port);
// Check connection

if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
} else {
	$sql = 'select * from customers;';
	$result = $conn->query($sql);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
	<link rel="stylesheet" href="styles/font-awesome-4.7.0/css/font-awesome.min.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="styles/magnific-popup/magnific-popup.min.css">
	<link rel="stylesheet" href="styles/style.css">
	<link rel="stylesheet" href="styles/responsive-index.css">
	<title>FinLoans</title>
</head>

<body>

	<!--Home -->
	<div class="home">
		<div class="home-overlay">
			<div class="home-overlay-content">
				<div class="home-overlay-content-heading ">
					<h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, nulla!</h1>
					<a href="#customers" class="btn home-overlay-btn">Transaction</a>
				</div>

			</div>

			<!-- Header -->
			<div class="fixed">
				<div class="navbar">
					<ul>
						<li class=""><a href="./index.php"><img src="./images/logo.png" alt="" class="menu-item"></a></li>
					</ul>
					<ul class="menu">
						<li class="menu-item"><a href="./index.php" class="menu-item-link">home</a></li>
						<li class="menu-item"><a href="#customers" class="menu-item-link">Users</a></li>



				</div>



			</div>
		</div>
	</div>
	<!-- Customers -->
	<div id="customers">
		<h2 style="text-align: center;">Customers</h2>
		<table>
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>DOB</th>
				<th>Balance</th>
				<th>City</th>
				<th>Transaction</th>
			</tr>
			<?php
			if ($result) {
				while ($row = $result->fetch_assoc()) {
			?>
					<tr>
						<td> <?= $row['id'] ?></td>
						<td> <a href="./profile.php?id=<?= $row['id'] ?>"> <?= $row['name'] ?></a> </td>
						<td> <?= $row['email'] ?></td>
						<td> <?= $row['phone'] ?></td>
						<td> <?= $row['dob'] ?></td>
						<td> <?= $row['balance'] ?></td>
						<td> <?= $row['city'] ?></td>
						<td> <a href="transfer.php?id=<?= $row['id'] ?>" class="btn-dark"> Tranfer</a></td>
					</tr>
			<?php
				}
			}
			?>


		</table>

	</div>
	<!--Contact-footer-->
	<div class="contact-footer">
		<div class="container">
			<div class="contact-footer-details">
				<div class="contact-footer-details-address">
					<ul class="contact-footer-details-physical-address">
						<li class="contact-footer-details-list"><img src="./images/footer_logo.png" alt=""></li>
						<li class="contact-footer-details-list">

						</li>
						<li class="contact-footer-details-list"><a href="#" class="contact-details-link">+91 9321545815</a></li>
						<li class="contact-footer-details-list"><a href="#" class="contact-details-link">contact@bank.com</a></li>
					</ul>
					<ul class="contact-footer-details-social-address">
						<li class="contact-footer-details-list"><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li class="contact-footer-details-list"><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li class="contact-footer-details-list"><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li class="contact-footer-details-list"><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
						<li class="contact-footer-details-list"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
					</ul>
				</div>
				<div class="contact-footer-details-links">
					<h6 class="contact-details-title">Useful Links</h6>
					<ul>
						<li class="contact-footer-details-list"><a href="./index.php" class="contact-footer-details-link">Home</a></li>
						<li class="contact-footer-details-list"><a href="./loan-details.php" class="contact-footer-details-link">Users</a></li>

					</ul>
				</div>
				<div class="contact-footer-details-subscribe">
					<h6 class="contact-details-title">Suscribe</h6>
					<input type="text" placeholder="Enter your mail" class="contact-footer-details-subscribe-email">
					<input type="submit" value="Subscribe" class="contact-footer-details-subscribe-submit">

				</div>
			</div>
		</div>
	</div>
</body>
<script src="./scripts/jquery.js"></script>
<!-- Magnific Popup -->
<script src="scripts/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- waypoints -->
<script src="scripts/waypoints/jquery.waypoints.min.js"></script>
<script src="scripts/app.js"></script>
<script src="scripts/alert.js"></script>

</html>